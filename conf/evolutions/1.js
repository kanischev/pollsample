// --- !Ups
db.users.insert({"id": 1,
                 "email": "kanischev@gmail.com",
                 "firstName": "Albert",
                 "lastName": "Kanischev",
                 "birthDate": null,
                 "gender": "Male",
                 "active": true,
                 "roles": ["User", "Admin"],
                 "password": "1ce8b5018391457553ea90ac61e6e900303d243e9eb6310bde66b7a33adf4fb471892fbbebddac06d6b49a06b9bc0b690d363ec877a8a88517cc713d308ce155",
                 "salt": "f693f8bf-80ff-448c-8c76-41532fd3847e"})

// --- !Downs
db.users.remove()