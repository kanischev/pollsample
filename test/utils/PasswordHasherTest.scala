package utils

import org.specs2.Specification

class PasswordHasherTest extends Specification {def is = s2"""
  Admin hashed \'admin\' password is correct with AdminHasher   $checkPredefinedPasswordHash"""

  def checkPredefinedPasswordHash = {
    val passwordHasher = new PasswordHasher {}
    val passwordHash = passwordHasher.generateHash("admin", Some("f693f8bf-80ff-448c-8c76-41532fd3847e"))
    passwordHash must_== "1ce8b5018391457553ea90ac61e6e900303d243e9eb6310bde66b7a33adf4fb471892fbbebddac06d6b49a06b9bc0b690d363ec877a8a88517cc713d308ce155"
  }
}
