name := """poll-master"""

version := "0.1-SNAPSHOT"

scalaVersion := "2.11.6"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

pipelineStages := Seq(uglify, digest, gzip)

pipelineStages in Assets := Seq()

pipelineStages := Seq(uglify, digest, gzip)

DigestKeys.algorithms += "sha1"

UglifyKeys.uglifyOps := { js =>
  Seq((js.sortBy(_._2), "concat.min.js"))
}

resolvers ++= Seq("Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
  "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases")

resolvers += Resolver.mavenLocal

libraryDependencies ++= Seq(
  specs2 % Test,
  "com.google.inject" % "guice" % "4.0",
  "javax.inject" % "javax.inject" % "1",
  "com.scalableminds" %% "play-mongev" % "0.4.0",
  "org.reactivemongo" %% "play2-reactivemongo" % "0.11.6.play24",
  "net.liftweb" %% "lift-json" % "2.6",
  "org.webjars" % "bootstrap" % "3.3.4",
  "org.webjars" % "angularjs" % "1.4.3-1",
  "org.webjars" % "angular-ui-bootstrap" % "0.13.0",
  "org.mockito" % "mockito-core" % "1.10.19" % "test")

routesGenerator := InjectedRoutesGenerator
scalacOptions in Test ++= Seq("-Yrangepos")