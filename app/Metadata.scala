/**
 * Created by Kaa 
 * on 19.08.2015 at 23:08.
 */
trait Metadata {
  object Roles {
    val Admin = "Admin"
    val User = "User"
  }

  object Gender {
    val Male = "Male"
    val Female = "Female"
  }
}
