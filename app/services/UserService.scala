package services

import com.google.inject.{ImplementedBy, Inject, Singleton}
import models._
import models.JsonFormats._
import play.modules.reactivemongo.json._
import play.api.libs.json.Json
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.Cursor
import scala.concurrent.{ExecutionContext, Future}

/**
*  Created by Kaa
*  on 19.08.2015 at 22:30.
*/
@ImplementedBy(classOf[UserServiceImpl])
trait UserService {
  def getUsers()(implicit executionContext: ExecutionContext): Future[Seq[User]]

  def getUser(id: Long): Option[User]

  def getUserBy(login: String): Option[User]

  def createUser(user: User): Option[User]

  def deleteUser(user: User): Boolean
}

class UserServiceImpl @Inject()(val reactiveMongoApi: ReactiveMongoApi) extends UserService with MongoService {
  implicit
  def usersCollection = collection("users")

  override def getUsers()(implicit executionContext: ExecutionContext) = {
    val cursor: Cursor[User] = usersCollection.
      find(Json.obj("active" -> true)).
      sort(Json.obj("created" -> -1)).
      cursor[User]()
    cursor.collect[List]()
  }

  override def deleteUser(user: User): Boolean = ???

  override def getUser(id: Long): Option[User] = ???

  override def createUser(user: User): Option[User] = ???

  override def getUserBy(login: String): Option[User] = ???
}