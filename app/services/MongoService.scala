package services

import play.modules.reactivemongo.{ReactiveMongoComponents, ReactiveMongoApi}
import play.modules.reactivemongo.json.collection.JSONCollection

trait MongoService extends ReactiveMongoComponents {
  /** Returns the current instance of the driver. */
  def driver = reactiveMongoApi.driver

  /** Returns the current MongoConnection instance (the connection pool manager). */
  def connection = reactiveMongoApi.connection

  /** Returns the default database (as specified in `application.conf`). */
  def db = reactiveMongoApi.db

  def collection(collectionName: String): JSONCollection = db.collection[JSONCollection](collectionName)
}
