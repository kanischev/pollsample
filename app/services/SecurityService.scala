package services

import models.UserSession

/**
 * Created by Kaa
 * on 19.08.2015 at 22:34.
 */
trait SecurityService {
  def authenticateUser(login: String, password: String): Either[UserSession, String]
}
