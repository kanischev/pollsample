package models

import java.util.Date

case class User( id: Long,
                 email: String,
                 firstName: String,
                 lastName: String,
                 birthDate: Date,
                 gender: String,
                 active: Boolean,
                 password: String,
                 salt: String,
                 roles: Seq[String])

object JsonFormats {
  import play.api.libs.json.Json

  // Generates Writes and Reads for Feed and User thanks to Json Macros
  implicit val userFormat = Json.format[User]
}