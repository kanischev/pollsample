package models

import org.joda.time.DateTime

/**
 * Created by Kaa 
 * on 19.08.2015 at 22:55.
 */
case class UserSession( sid: String,
                        userId: Long,
                        createTime: DateTime,
                        validTill: DateTime) {

}
