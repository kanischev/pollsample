package models

/**
 * Created by Kaa 
 * on 19.08.2015 at 22:55.
 */
trait MongoEntity {
  def id: String
  def collectionName: String
}
