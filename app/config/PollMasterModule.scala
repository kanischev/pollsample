package config

import com.google.inject.AbstractModule
import services.{SimpleUUIDGenerator, UUIDGenerator}

class PollMasterModule extends AbstractModule{
  override def configure(): Unit = {
    bind(classOf[UUIDGenerator]) to classOf[SimpleUUIDGenerator]
  }
}
