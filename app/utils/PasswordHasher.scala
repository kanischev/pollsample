package utils

/**
 * Created by Kaa 
 * on 20.08.2015 at 00:31.
 */
import java.security.MessageDigest
import java.util.UUID
import org.apache.commons.lang3.RandomStringUtils

trait PasswordHasher {

  def generateSalt = UUID.randomUUID().toString

  def generateHash(input: String, salt: Option[String]): String =
    generateByteArrayHash((salt.map(generateHash(_, None)).getOrElse("") + input).getBytes)

  def generateByteArrayHash(data: Array[Byte]) = {
    val md = MessageDigest.getInstance("SHA-512")
    md.digest(data).map(0xFF & _).map("%02x".format(_)).foldLeft("")(_ + _)
  }
}

trait PasswordGenerator {
  def generatePassword(length: Int = 10) = {
    RandomStringUtils.randomAlphanumeric(length)
  }
}

/**
 * date: 14.05.12
 * author: Kaa
 */

trait MD5HexGenerator {
  def generateMd5Hex(input: String): String = generateMd5Hex(input.getBytes)

  def generateMd5Hex(data: Array[Byte]) = {
    val md = MessageDigest.getInstance("MD5")
    md.digest(data).map(0xFF & _).map("%02x".format(_)).foldLeft("")(_ + _)
  }
}